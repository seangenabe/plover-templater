from re import I
from plover_templater.templater import Templater
from plover import log
from pprint import pprint

class EngineContext:

  def __init__(self, engine):
    self.engine = engine

  def start(self):
    self.engine.hook_connect(
      "dictionaries_loaded",
      self.on_dictionaries_loaded
    )
    self.on_dictionaries_loaded(self.engine.dictionaries)

  def on_dictionaries_loaded(self, dictionaries):
    def getSources():
          seenThis = False
          for dict2 in dictionaries.dicts:
            if seenThis:
              yield dict2
            else:
              if dict is dict2:
                seenThis = True
    for dict in dictionaries.dicts:
      if isinstance(dict, Templater):
        sources = list(getSources())
        dict.set_sources(sources)

  def stop(self):
    pass

def p(str, val):
  print(str, end=" ")
  pprint(val)
