from plover.steno_dictionary import StenoDictionary, StenoDictionaryCollection
from plover import log
from pprint import pprint
from re import sub, findall
from itertools import product
from plover.steno import normalize_steno

# try:
#   import simplejson as json
# except ImportError:
#   import json
import ruamel.yaml

class Templater(StenoDictionary):
  sources = list()
  col = None
  readonly = True

  def _load(self, filename):
    self.filename = filename
    #with open(filename, "rb") as fp:
    #  contents = fp.read()
    #contents = contents.decode("utf-8")
    # self.templates = dict(json.loads(contents))
    yaml = ruamel.yaml.YAML(typ = "safe")

    with open(filename, "r", encoding = "utf-8") as f:
      data = yaml.load(f)
    self.templates = dict(data)

    self.update_dictionary_entries()

  def save(self):
    log.warning("Cannot save")

  def set_sources(self, sources):
    self.sources = sources
    self.update_dictionary_entries()

  def update_dictionary_entries(self):
    self.col = StenoDictionaryCollection(self.sources)
    self._dict.clear()
    self.reverse.clear()
    self.casereverse.clear()
    self._longest_key = 0

    if not self.sources:
      return
    for template, translation in self.templates.items():
      needles = findall(r'\$\<([^)]*?)\>', template)
      # needle => stroke
      needle_replacements = {
        # Attempt reverse lookup on provided needle
        needle: self.lookup_needle(needle, template) for needle in needles
      }
      if any(
        len(replacement) == 0 for replacement in needle_replacements.values()
      ):
        log.info(
          f"Ignoring template {template} because of missing replacements."
        )
        continue
      for replacement in product(*needle_replacements.values()):
        replacement_queue = list(replacement)
        new_str = sub(
          r'\$\<([^)]*?)\>',
          lambda match: "/".join(replacement_queue.pop(0)),
          template
        )
        steno = normalize_steno(new_str)
        self._dict[steno] = translation
        self.reverse[translation].append(steno)
        self.casereverse[translation.lower()].append(steno)
        steno_len = len(steno)
        if steno_len > self._longest_key:
          self._longest_key = steno_len

  def lookup_needle(self, needle, template):
    ret = self.col.reverse_lookup(needle)
    if len(ret) == 0:
      log.info(f"Needle {needle} was not found from template {template}.")
    return ret

def p(str, val):
  print(str, end=" ")
  pprint(val, depth=5)
